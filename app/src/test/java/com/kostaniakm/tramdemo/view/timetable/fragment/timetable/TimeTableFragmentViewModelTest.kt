package com.kostaniakm.tramdemo.view.timetable.fragment.timetable

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kostaniakm.tramdemo.data.model.TramTimeData
import com.kostaniakm.tramdemo.data.model.TramTimeDataResponse
import com.kostaniakm.tramdemo.data.model.TramTimePairData
import com.kostaniakm.tramdemo.data.repository.TramStopDataRepository
import com.kostaniakm.tramdemo.scheduler.BaseSchedulerProvider
import com.kostaniakm.tramdemo.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class TimeTableFragmentViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var tramStopDataRepository: TramStopDataRepository
    private lateinit var baseSchedulerProvider: BaseSchedulerProvider

    private lateinit var underTestModel: TimeTableFragmentViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        baseSchedulerProvider = TrampolineSchedulerProvider()
    }

    @Test
    fun `when getTimeTableData busCode is correctly splited`() {
        every { tramStopDataRepository.getTramTimeTableData(any(), any(), any()) } returns Observable.just(
            prepareCorrectData()
        )
        setupModel()

        underTestModel.getTimeTableData("sampleLane", "604009")

        verify { tramStopDataRepository.getTramTimeTableData(eq("sampleLane"), eq("09"), eq("6040")) }
    }

    @Test
    fun `when data is returned from repository check for sorted results`() {
        every { tramStopDataRepository.getTramTimeTableData(any(), any(), any()) } returns Observable.just(
            prepareCorrectData()
        )
        setupModel()

        underTestModel.getTimeTableData("sampleLane", "sampleCode")

        assertEquals(underTestModel.getTimeTableLiveData().value!!.size, 5)
        assertEquals(underTestModel.getTimeTableLiveData().value!![0].keyHour, "1")
        assertEquals(underTestModel.getTimeTableLiveData().value!![1].keyHour, "6")
        assertEquals(underTestModel.getTimeTableLiveData().value!![2].keyHour, "12")
        assertEquals(underTestModel.getTimeTableLiveData().value!![3].keyHour, "15")
        assertEquals(underTestModel.getTimeTableLiveData().value!![4].keyHour, "21")
    }

    @Test
    fun `when data is returned from repository check for correct results`() {
        every { tramStopDataRepository.getTramTimeTableData(any(), any(), any()) } returns Observable.just(
            prepareCorrectData()
        )
        setupModel()

        underTestModel.getTimeTableData("sampleLane", "sampleCode")

        assertEquals(underTestModel.getTimeTableLiveData().value!!.size, 5)
        assertEquals(underTestModel.getTimeTableLiveData().value!![0].list.size, 2)
        assertEquals(underTestModel.getTimeTableLiveData().value!![1].list.size, 1)
        assertEquals(underTestModel.getTimeTableLiveData().value!![2].list.size, 1)
        assertEquals(underTestModel.getTimeTableLiveData().value!![3].list.size, 1)
        assertEquals(underTestModel.getTimeTableLiveData().value!![4].list.size, 2)
    }

    @Test
    fun `when error returned from repository should call error live data`() {
        val error = Throwable("Connection error")
        every { tramStopDataRepository.getTramTimeTableData(any(), any(), any()) } returns Observable.error(error)
        setupModel()

        underTestModel.getTimeTableData("sampleLane", "sampleCode")

        assertEquals(underTestModel.getSingleLiveErrorEvent().value!!, error)
        assertEquals(underTestModel.getTimeTableLiveData().value, null)
    }

    private fun setupModel() {
        underTestModel = TimeTableFragmentViewModel(tramStopDataRepository, baseSchedulerProvider)
    }

    private fun prepareCorrectData(): TramTimeDataResponse {
        return TramTimeDataResponse(
            listOf(
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "21:35:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "01:15:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "21:00:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "06:32:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "12:38:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "01:19:34", key = TramTimeData.TIME_KEY)
                    )
                ),
                TramTimeData(
                    listOf(
                        TramTimePairData(value = "sample1", key = "key1"),
                        TramTimePairData(value = "sample2", key = "key2"),
                        TramTimePairData(value = "15:14:34", key = TramTimeData.TIME_KEY)
                    )
                )
            )
        )
    }

}