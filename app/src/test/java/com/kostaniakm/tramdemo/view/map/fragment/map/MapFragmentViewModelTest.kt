package com.kostaniakm.tramdemo.view.map.fragment.map

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kostaniakm.tramdemo.data.model.TramStopData
import com.kostaniakm.tramdemo.data.model.TramStopDataResponse
import com.kostaniakm.tramdemo.data.repository.TramStopDataRepository
import com.kostaniakm.tramdemo.scheduler.BaseSchedulerProvider
import com.kostaniakm.tramdemo.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MapFragmentViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var tramStopDataRepository: TramStopDataRepository
    private lateinit var baseSchedulerProvider: BaseSchedulerProvider

    private lateinit var underTestModel: MapFragmentViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        baseSchedulerProvider = TrampolineSchedulerProvider()
    }

    @Test
    fun `when data is returned from repository check for returned data`() {
        every { tramStopDataRepository.getTramStopGeoData(any()) } returns Observable.just(prepareCorrectData())
        setupModel()

        underTestModel.loadTramStopGeoData("sample")

        assertEquals(underTestModel.getTramStopGeoData().value?.size, 4)
    }

    @Test
    fun `when data is returned from repository check for correct bound calculation`() {
        every { tramStopDataRepository.getTramStopGeoData(any()) } returns Observable.just(prepareCorrectData())
        setupModel()

        underTestModel.loadTramStopGeoData("sample")

        assertEquals(
            underTestModel.getTramStopBoundData().value!!.northeast.latitude,
            52.266736,
            DELTA_FOR_DOUBLE_ASSERT
        )
        assertEquals(
            underTestModel.getTramStopBoundData().value!!.northeast.longitude,
            21.011332,
            DELTA_FOR_DOUBLE_ASSERT
        )
        assertEquals(
            underTestModel.getTramStopBoundData().value!!.southwest.latitude,
            52.230462,
            DELTA_FOR_DOUBLE_ASSERT
        )
        assertEquals(
            underTestModel.getTramStopBoundData().value!!.southwest.longitude,
            20.962833,
            DELTA_FOR_DOUBLE_ASSERT
        )
    }

    @Test
    fun `when error returned from repository should call error live data`() {
        val error = Throwable("Connection error")
        every { tramStopDataRepository.getTramStopGeoData(any()) } returns Observable.error(error)
        setupModel()

        underTestModel.loadTramStopGeoData("sample")

        assertEquals(underTestModel.getSingleLiveErrorEvent().value!!, error)
        assertEquals(underTestModel.getTramStopBoundData().value, null)
        assertEquals(underTestModel.getTramStopGeoData().value, null)
    }

    private fun setupModel() {
        underTestModel = MapFragmentViewModel(tramStopDataRepository, baseSchedulerProvider)
    }

    private fun prepareCorrectData(): TramStopDataResponse {
        return TramStopDataResponse(
            listOf(
                TramStopData("Banacha", 52.230462, 35, 21.011332, "Centrum 07", 701307),
                TramStopData("Banacha", 52.253606, 35, 20.997271, "Conrada 09", 701307),
                TramStopData("Banacha", 52.251351, 35, 20.985354, "Pl. Grunwaldzki 07", 701307),
                TramStopData("Banacha", 52.266736, 35, 20.962833, "Al. Reymonta 08", 701307)
            )
        )
    }

    companion object {
        private const val DELTA_FOR_DOUBLE_ASSERT = 0.001
    }

}