package com.kostaniakm.tramdemo.data.model

import com.kostaniakm.tramdemo.data.model.TramTimeData.Companion.TIME_KEY
import org.junit.Assert.assertEquals
import org.junit.Test

class TramTimeDataTest {

    @Test
    fun `getHourFromTramTimeData() returns correct hour`() {
        val underTest = prepareTramTimeData()

        assertEquals(underTest.getHourFromTramTimeData(), "21")
    }

    @Test
    fun `getMinutesFromTramTimeData() returns correct minute`() {
        val underTest = prepareTramTimeData()

        assertEquals(underTest.getMinutesFromTramTimeData(), "14")
    }

    private fun prepareTramTimeData() =
        TramTimeData(
            listOf(
                TramTimePairData(value = "15:12:31", key = "key1"),
                TramTimePairData(value = "14:11:32", key = "key2"),
                TramTimePairData(value = "21:14:34", key = TIME_KEY)
            )
        )

}