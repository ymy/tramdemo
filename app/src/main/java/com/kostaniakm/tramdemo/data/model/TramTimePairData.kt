package com.kostaniakm.tramdemo.data.model

data class TramTimePairData(val value : String, val key : String)