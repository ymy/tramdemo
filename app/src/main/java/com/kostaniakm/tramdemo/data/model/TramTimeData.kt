package com.kostaniakm.tramdemo.data.model

import com.kostaniakm.tramdemo.data.model.TramTimeData.Companion.TIME_KEY
import com.kostaniakm.tramdemo.data.model.TramTimeData.Companion.formatter
import java.text.SimpleDateFormat
import java.util.*

data class TramTimeData(
    val values: List<TramTimePairData>
) {

    companion object {
        const val TIME_KEY = "czas"
        val formatter = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
    }
}

fun TramTimeData.getHourFromTramTimeData(): String {
    return this.getCalendarFromTramTimeData().get(Calendar.HOUR_OF_DAY).toString()
}

fun TramTimeData.getMinutesFromTramTimeData(): String? {
    return this.getCalendarFromTramTimeData().get(Calendar.MINUTE).toString()
}

fun TramTimeData.getCalendarFromTramTimeData(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = formatter.parse(this.values.find { it.key == TIME_KEY }?.value)
    return calendar
}