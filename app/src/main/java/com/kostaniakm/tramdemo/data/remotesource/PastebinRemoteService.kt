package com.kostaniakm.tramdemo.data.remotesource

import com.kostaniakm.tramdemo.data.model.TramStopDataResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface PastebinRemoteService {

    @GET("raw/{id}")
    fun getTramStopsData(@Path("id") id : String) : Observable<TramStopDataResponse>

}