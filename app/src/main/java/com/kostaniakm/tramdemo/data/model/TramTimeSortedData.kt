package com.kostaniakm.tramdemo.data.model

data class TramTimeSortedData(val keyHour: String, val list: List<TramTimeData>) : Comparable<TramTimeSortedData> {

    override fun compareTo(other: TramTimeSortedData): Int {
        return when {
            Integer.valueOf(keyHour) > Integer.valueOf(other.keyHour) -> 1
            Integer.valueOf(keyHour) < Integer.valueOf(other.keyHour) -> -1
            else -> 0
        }
    }

    override fun toString(): String {
        var result = ""
        result += "$keyHour  |  "
        list.forEach { tramTimeData: TramTimeData ->
            result += tramTimeData.getMinutesFromTramTimeData() + " "
        }
        result += "\n"
        return result
    }
}