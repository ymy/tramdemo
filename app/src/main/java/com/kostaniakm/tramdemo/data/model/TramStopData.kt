package com.kostaniakm.tramdemo.data.model

import android.location.Location

data class TramStopData(
    val direction: String,
    val lat: Double,
    val line: Int,
    val lon: Double,
    val name: String,
    val stopId: Int
)

fun TramStopData.getLocation(): Location = Location("").apply {
    longitude = lon
    latitude = lat
}
