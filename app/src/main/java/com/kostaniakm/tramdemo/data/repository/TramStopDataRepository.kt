package com.kostaniakm.tramdemo.data.repository

import com.kostaniakm.tramdemo.BuildConfig
import com.kostaniakm.tramdemo.data.remotesource.PastebinRemoteService
import com.kostaniakm.tramdemo.data.remotesource.TramTimeTableRemoteService
import javax.inject.Inject

class TramStopDataRepository @Inject constructor(
    private val pastebinRemoteService: PastebinRemoteService,
    private val tramTimeTableRemoteService: TramTimeTableRemoteService
) {

    fun getTramStopGeoData(id: String) = pastebinRemoteService.getTramStopsData(id)

    fun getTramTimeTableData(
        lane: String,
        busStopNr: String,
        busStopId: String
    ) = tramTimeTableRemoteService.getTramStopsData(BuildConfig.KEY_UM_API, lane, busStopNr, busStopId)

}