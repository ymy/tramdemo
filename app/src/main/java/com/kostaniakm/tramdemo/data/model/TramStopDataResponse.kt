package com.kostaniakm.tramdemo.data.model

data class TramStopDataResponse(val stops : List<TramStopData>)