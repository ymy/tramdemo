package com.kostaniakm.tramdemo.data.remotesource

import com.kostaniakm.tramdemo.data.model.TramTimeDataResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface TramTimeTableRemoteService {

    @GET("action/dbtimetable_get/?id=e923fa0e-d96c-43f9-ae6e-60518c9f3238")
    fun getTramStopsData(
        @Query("apikey") apiKey: String,
        @Query("line") lane: String,
        @Query("busstopNr") busStopNr: String,
        @Query("busstopId") busStopId: String
    ): Observable<TramTimeDataResponse>

}