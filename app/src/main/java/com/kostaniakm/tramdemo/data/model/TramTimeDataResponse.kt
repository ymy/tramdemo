package com.kostaniakm.tramdemo.data.model

data class TramTimeDataResponse(val result: List<TramTimeData>)