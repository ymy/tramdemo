package com.kostaniakm.tramdemo.view.timetable.fragment.timetable

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kostaniakm.tramdemo.data.model.TramTimeData
import com.kostaniakm.tramdemo.data.model.TramTimeSortedData
import com.kostaniakm.tramdemo.data.model.getHourFromTramTimeData
import com.kostaniakm.tramdemo.data.repository.TramStopDataRepository
import com.kostaniakm.tramdemo.livedata.SingleLiveEvent
import com.kostaniakm.tramdemo.scheduler.BaseSchedulerProvider
import com.kostaniakm.tramdemo.viewmodel.BaseViewModel
import javax.inject.Inject

class TimeTableFragmentViewModel @Inject constructor(
    private val tramStopDataRepository: TramStopDataRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) :
    BaseViewModel() {

    private val timeTableData = MutableLiveData<MutableList<TramTimeSortedData>>()
    private val errorData = SingleLiveEvent<Throwable>()

    init {
        //empty for now =)
    }

    fun getTimeTableData(lane: String, busCode: String) {
        compositeDisposable.add(
            tramStopDataRepository.getTramTimeTableData(
                lane,
                getBusStopNrFromBusCode(busCode),
                getBusStopIdFromBusCode(busCode)
            )
                .flatMapIterable { t -> t.result }
                .groupBy { t: TramTimeData -> t.getHourFromTramTimeData() }
                .flatMapSingle { t -> t.toList() }
                .map { t: MutableList<TramTimeData> ->
                    TramTimeSortedData(
                        t[0].getHourFromTramTimeData(), t
                    )
                }
                .toSortedList()
                .subscribeOn(baseSchedulerProvider.io())
                .observeOn(baseSchedulerProvider.ui())
                .subscribe(
                    { t: MutableList<TramTimeSortedData> ->
                        timeTableData.postValue(t)
                    },
                    { t: Throwable? ->
                        errorData.value = t
                    }
                )
        )
    }

    fun getTimeTableLiveData(): LiveData<MutableList<TramTimeSortedData>> = timeTableData

    fun getSingleLiveErrorEvent(): SingleLiveEvent<Throwable> = errorData

    private fun getBusStopNrFromBusCode(busCode: String) = busCode.substring(BUS_STOP_NR_INDEX_START, BUS_STOP_NR_INDEX_STOP)

    private fun getBusStopIdFromBusCode(busCode: String) = busCode.substring(BUS_STOP_ID_INDEX_START, BUS_STOP_ID_INDEX_STOP)

    companion object {
        private const val BUS_STOP_NR_INDEX_START = 4
        private const val BUS_STOP_NR_INDEX_STOP = 6
        private const val BUS_STOP_ID_INDEX_START = 0
        private const val BUS_STOP_ID_INDEX_STOP = 4
    }

}