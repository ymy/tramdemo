package com.kostaniakm.tramdemo.view.map.fragment.menu

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.kostaniakm.tramdemo.R
import com.kostaniakm.tramdemo.view.base.BaseFragment
import kotlinx.android.synthetic.main.menu_fragment.*

class MenuFragment : BaseFragment() {

    lateinit var navigation: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigation = Navigation.findNavController(view)
        goToMapBtn.setOnClickListener {
            if (isPermissionGrantedForLocation()) {
                goToMap()
            } else {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_REQUEST
                )
            }
        }
        closeBtn.setOnClickListener { activity?.finish() }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        goToMap()
    }

    private fun goToMap() {
        // In real application these two (pastebinId, lineNumber) should be mutable parameters passed to the next screen. It is easy way to expand this feature for any other lines.
        navigation.navigate(MenuFragmentDirections.actionMenuFragmentToMapFragment("S3QwgEbf", 35))
    }

    companion object {
        private const val PERMISSION_REQUEST: Int = 2312
    }

}