package com.kostaniakm.tramdemo.view.timetable.fragment.timetable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.kostaniakm.tramdemo.R
import com.kostaniakm.tramdemo.view.base.BaseFragment
import com.kostaniakm.tramdemo.view.timetable.TimeTableActivityArgs
import kotlinx.android.synthetic.main.time_table_fragment.*
import javax.inject.Inject

class TimeTableFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: TimeTableFragmentViewModel
    private var snackBar: Snackbar? = null

    //this is ugly - needs some time to figure out better solution.
    //https://stackoverflow.com/questions/50334550/navigation-architecture-component-passing-argument-data-to-the-startdestination
    private val lineNumber: Int by lazy { TimeTableActivityArgs.fromBundle(requireActivity().intent.extras).lineNumber }
    private val stopId: String by lazy { TimeTableActivityArgs.fromBundle(requireActivity().intent.extras).stopId }
    private val stopName: String by lazy { TimeTableActivityArgs.fromBundle(requireActivity().intent.extras).stopName }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, this.viewModelFactory).get(TimeTableFragmentViewModel::class.java)
        viewModel.getTimeTableData(lineNumber.toString(), stopId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.time_table_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getSingleLiveErrorEvent().observe(viewLifecycleOwner, Observer { _ ->
            snackBar = Snackbar.make(view, getString(R.string.map_error), Snackbar.LENGTH_INDEFINITE).apply {
                setAction(getString(R.string.map_retry)) { viewModel.getTimeTableData(lineNumber.toString(), stopId) }
                show()
            }
        })
        busName.text = stopName
        viewModel.getTimeTableLiveData().observe(viewLifecycleOwner, Observer { t ->
            //:(
            //Replace with recycleview + databinding, just do not have time for it, most important is to get correct data
            var result = ""
            t.forEach(action = { tramTimeSortedData ->
                result += tramTimeSortedData.toString()
            })
            busTimeTable.text = result
        })
    }

    override fun onDestroy() {
        snackBar?.dismiss()
        super.onDestroy()
    }

}