package com.kostaniakm.tramdemo.view.map.fragment.map

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.kostaniakm.tramdemo.R
import com.kostaniakm.tramdemo.data.model.TramStopData
import com.kostaniakm.tramdemo.location.LocationProvider
import com.kostaniakm.tramdemo.view.base.BaseFragment
import javax.inject.Inject

class MapFragment : BaseFragment(), OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MapFragmentViewModel
    private lateinit var googleMap: GoogleMap
    private lateinit var mapView: MapView
    private var snackBar: Snackbar? = null
    private val pastebindId: String by lazy { MapFragmentArgs.fromBundle(arguments).pastebinId }
    private val lineNumber: Int by lazy { MapFragmentArgs.fromBundle(arguments).lineNumber }
    private lateinit var navigation: NavController
    private val markers = mutableListOf<Marker>()
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(location: LocationResult?) {
            location?.locations?.first()?.let { viewModel.setUserLocation(it) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, this.viewModelFactory).get(MapFragmentViewModel::class.java)
        viewModel.loadTramStopGeoData(pastebindId)
        LocationProvider(requireActivity(), lifecycle, locationCallback)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.map_fragment, container, false)
        mapView = view.findViewById(R.id.mapView)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        navigation = Navigation.findNavController(view)

        viewModel.getSingleLiveErrorEvent().observe(viewLifecycleOwner, Observer { _ ->
            snackBar = Snackbar.make(view, getString(R.string.map_error), Snackbar.LENGTH_INDEFINITE).apply {
                setAction(getString(R.string.map_retry)) { viewModel.loadTramStopGeoData(pastebindId) }
                show()
            }
        })
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map
        googleMap.uiSettings.isMyLocationButtonEnabled = true

        @SuppressLint("MissingPermission")
        if (isPermissionGrantedForLocation()) {
            googleMap.isMyLocationEnabled = true
        }

        viewModel.getTramStopGeoData().observe(viewLifecycleOwner, Observer { data ->
            drawMarkersOnMap(data)
        })

        viewModel.getTramStopBoundData().observe(viewLifecycleOwner, Observer { data ->
            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngBounds(
                    data,
                    CAMERA_PADDING
                )
            )
        })

        viewModel.getNearestStopGeoData().observe(viewLifecycleOwner, Observer { data ->
            selectNearestMarker(data)
        })

        googleMap.setOnInfoWindowClickListener {
            navigation.navigate(
                MapFragmentDirections.actionMapFragmentToTimeTableActivity(
                    lineNumber,
                    it.snippet,
                    it.title
                )
            )
        }
    }

    private fun drawMarkersOnMap(data: List<TramStopData>) {
        googleMap.apply {
            clear()
            markers.clear()
            data.forEach {
                markers.add(
                    addMarker(
                        MarkerOptions()
                            .position(LatLng(it.lat, it.lon))
                            .snippet(it.stopId.toString())
                            .title(it.name)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue))
                    )
                )
            }
        }
    }

    private fun selectNearestMarker(data: TramStopData?) {
        if (data != null) {
            markers.forEach(action = { marker ->
                if(marker.title == data.name)
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red))
                else
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue))
            })
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        snackBar?.dismiss()
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    companion object {
        private const val CAMERA_PADDING: Int = 100
    }

}