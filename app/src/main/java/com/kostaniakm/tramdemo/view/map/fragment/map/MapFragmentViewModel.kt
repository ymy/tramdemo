package com.kostaniakm.tramdemo.view.map.fragment.map

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.kostaniakm.tramdemo.data.model.TramStopData
import com.kostaniakm.tramdemo.data.model.TramStopDataResponse
import com.kostaniakm.tramdemo.data.model.getLocation
import com.kostaniakm.tramdemo.data.repository.TramStopDataRepository
import com.kostaniakm.tramdemo.livedata.SingleLiveEvent
import com.kostaniakm.tramdemo.scheduler.BaseSchedulerProvider
import com.kostaniakm.tramdemo.viewmodel.BaseViewModel
import io.reactivex.Observable
import javax.inject.Inject

class MapFragmentViewModel @Inject constructor(
    private val tramStopDataRepository: TramStopDataRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) :
    BaseViewModel() {

    private val tramStopGeoData = MutableLiveData<List<TramStopData>>()
    private val tramStopBoundData = MutableLiveData<LatLngBounds>()
    private val tramNearestGeoData = MutableLiveData<TramStopData>()
    private val errorData = SingleLiveEvent<Throwable>()

    init {
        //empty for now
    }

    fun setUserLocation(location: Location) {
        compositeDisposable.add(
            Observable.just(location)
                .map { it ->
                    tramStopGeoData.value?.reduce(operation = { acc: TramStopData, tramStopData: TramStopData ->
                        if (it.distanceTo(acc.getLocation()) < it.distanceTo(tramStopData.getLocation()))
                            acc else tramStopData
                    })
                }
                .subscribeOn(baseSchedulerProvider.io())
                .observeOn(baseSchedulerProvider.ui())
                .subscribe(
                    { t: TramStopData? ->
                        tramNearestGeoData.postValue(t)
                    },
                    {
                        //handle somehow?
                    }
                )
        )
    }

    fun loadTramStopGeoData(id: String) {
        compositeDisposable.add(
            tramStopDataRepository.getTramStopGeoData(id)
                .doOnNext {
                    val maxLon = it.stops.maxBy { tramStopData -> tramStopData.lon }?.lon ?: WARSAW.northeast.longitude
                    val maxLat = it.stops.maxBy { tramStopData -> tramStopData.lat }?.lat ?: WARSAW.northeast.latitude
                    val minLon = it.stops.minBy { tramStopData -> tramStopData.lon }?.lon ?: WARSAW.southwest.longitude
                    val minLat = it.stops.minBy { tramStopData -> tramStopData.lat }?.lat ?: WARSAW.southwest.latitude
                    tramStopBoundData.postValue(
                        LatLngBounds(
                            LatLng(minLat, minLon), LatLng(maxLat, maxLon)
                        )
                    )
                }
                .flatMapIterable { t: TramStopDataResponse -> t.stops }
                .toList()
                .subscribeOn(baseSchedulerProvider.io())
                .observeOn(baseSchedulerProvider.ui())
                .subscribe(
                    { response: List<TramStopData> ->
                        tramStopGeoData.postValue(response)
                    },
                    { t: Throwable? ->
                        errorData.value = t
                    }
                )
        )
    }

    fun getTramStopGeoData(): LiveData<List<TramStopData>> = tramStopGeoData

    fun getNearestStopGeoData(): LiveData<TramStopData> = tramNearestGeoData

    fun getTramStopBoundData(): LiveData<LatLngBounds> = tramStopBoundData

    fun getSingleLiveErrorEvent(): SingleLiveEvent<Throwable> = errorData

    companion object {
        private val WARSAW = LatLngBounds(
            LatLng(52.141511, 20.880404), LatLng(52.305816, 21.140926)
        )
    }

}