package com.kostaniakm.tramdemo.di.module


import com.kostaniakm.tramdemo.view.map.fragment.map.MapFragment
import com.kostaniakm.tramdemo.view.map.fragment.menu.MenuFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class MainActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun provideMapFragment(): MapFragment

    @ContributesAndroidInjector
    abstract fun provideMenuFragment(): MenuFragment

}
