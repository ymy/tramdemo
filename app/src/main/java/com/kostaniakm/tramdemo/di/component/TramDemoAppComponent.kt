package com.kostaniakm.tramdemo.di.component

import com.kostaniakm.tramdemo.TramDemoApp
import com.kostaniakm.tramdemo.di.module.ActivityBuilder
import com.kostaniakm.tramdemo.di.module.ApplicationModule
import com.kostaniakm.tramdemo.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class,
        AndroidInjectionModule::class,
        NetworkModule::class,
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class]
)
interface TramDemoAppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: TramDemoApp): Builder

        fun build(): TramDemoAppComponent
    }

    fun inject(app: TramDemoApp)

}