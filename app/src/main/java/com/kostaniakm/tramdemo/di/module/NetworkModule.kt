package com.kostaniakm.tramdemo.di.module

import android.content.Context
import com.kostaniakm.tramdemo.BuildConfig
import com.kostaniakm.tramdemo.data.remotesource.PastebinRemoteService
import com.kostaniakm.tramdemo.data.remotesource.TramTimeTableRemoteService
import com.kostaniakm.tramdemo.scheduler.BaseSchedulerProvider
import com.kostaniakm.tramdemo.scheduler.SchedulerProvider
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    private val pastebinSourceUrl = BuildConfig.PASTEBIN_API_URL
    private val citySourceUrl = BuildConfig.UM_API_URL

    @Provides
    @Singleton
    fun provideOkHttpClient(context: Context): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .connectTimeout(15, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .cache(Cache(context.cacheDir, (5 * 1024 * 1024).toLong()))
        .build()


    @Provides
    @Singleton
    @Named("pastebinSourceRetrofit")
    fun providePastebinSourceRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(pastebinSourceUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    @Named("citySourceRetrofit")
    fun provideCitySourceRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(citySourceUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun providePastebinRemoteService(@Named("pastebinSourceRetrofit") retrofit: Retrofit): PastebinRemoteService =
        retrofit.create(PastebinRemoteService::class.java)

    @Provides
    @Singleton
    fun provideTramTimeTableRemoteService(@Named("citySourceRetrofit") retrofit: Retrofit): TramTimeTableRemoteService =
        retrofit.create(TramTimeTableRemoteService::class.java)

    @Provides
    @Singleton
    fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider()

}