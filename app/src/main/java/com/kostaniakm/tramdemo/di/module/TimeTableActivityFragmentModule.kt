package com.kostaniakm.tramdemo.di.module

import com.kostaniakm.tramdemo.view.timetable.fragment.timetable.TimeTableFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class TimeTableActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun provideTimeTableFragment(): TimeTableFragment

}