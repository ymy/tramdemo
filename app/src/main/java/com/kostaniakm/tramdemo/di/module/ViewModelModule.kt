package com.kostaniakm.tramdemo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kostaniakm.tramdemo.view.map.fragment.map.MapFragmentViewModel
import com.kostaniakm.tramdemo.view.timetable.fragment.timetable.TimeTableFragmentViewModel
import com.kostaniakm.tramdemo.viewmodel.DiViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: DiViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MapFragmentViewModel::class)
    abstract fun bindMapFragmentViewModel(userViewModel: MapFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TimeTableFragmentViewModel::class)
    abstract fun bindTimeTableFragmentViewModel(userViewModel: TimeTableFragmentViewModel): ViewModel

}