package com.kostaniakm.tramdemo.di.module

import android.content.Context
import com.kostaniakm.tramdemo.TramDemoApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideApplication(app: TramDemoApp): Context = app.applicationContext

}