package com.kostaniakm.tramdemo.di.module

import com.kostaniakm.tramdemo.view.map.MainActivity
import com.kostaniakm.tramdemo.view.timetable.TimeTableActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [TimeTableActivityFragmentModule::class])
    abstract fun bindTimeTableActivity(): TimeTableActivity

}